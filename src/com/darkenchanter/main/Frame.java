package com.darkenchanter.main;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import javax.imageio.IIOException;
import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileFilter;

import com.darkenchanter.resourceclasses.Giffer;
import com.darkenchanter.resourceclasses.Regex;

import at.dhyan.open_imaging.GifDecoder;
import at.dhyan.open_imaging.GifDecoder.GifImage;

public class Frame {
	JFrame frame = new JFrame();
	JPanel pane = new JPanel(new GridBagLayout()), PNGPane = new JPanel(new GridBagLayout()), PNGSpin = new JPanel(new GridBagLayout());
	GridBagConstraints c = new GridBagConstraints(); // For animated gif panel
	GridBagConstraints p = new GridBagConstraints(); // For static png panel
	GridBagConstraints a = new GridBagConstraints(); // For animated png panel
	Icon onHoverOnly = new ImageIcon(Frame.class.getResource("/com/darkenchanter/assets/ntho.gif"));
	JLabel ntho = new JLabel(onHoverOnly);
	JPanel nthoPn = new JPanel(new GridLayout());
	Icon onNotHoverOnly = new ImageIcon(Frame.class.getResource("/com/darkenchanter/assets/htno.gif"));
	JLabel htno = new JLabel(onNotHoverOnly);
	JPanel htnoPn = new JPanel(new GridLayout());
	Icon bothICO = new ImageIcon(Frame.class.getResource("/com/darkenchanter/assets/both.gif"));
	JLabel both = new JLabel(bothICO);
	JPanel bothPn = new JPanel(new GridLayout());
	Icon noneICO = new ImageIcon(Frame.class.getResource("/com/darkenchanter/assets/none.gif"));
	JLabel none = new JLabel(noneICO);
	JPanel nonePn = new JPanel(new GridLayout());
	JPanel trueMetaPane = new JPanel(new GridLayout());
	JPanel autoSpinPn = new JPanel(new GridLayout());
	JTabbedPane metaPane = new JTabbedPane();
	JTabbedPane demoPane = new JTabbedPane();
	JButton AP_normalBtn = new JButton("Choose Normal Icon");
	JButton AP_pressedBtn = new JButton("Choose Pressed Icon");
	JButton AP_constructBtn = new JButton("Save Icon");
	JPanel frames = new JPanel(new GridLayout(0, 1));
	JScrollPane scrollPane = new JScrollPane(frames);
	JButton chooseGif = new JButton("Choose GIF"), 
			chooseNml = new JButton("Choose Normal PNG"), 
			chooseHot = new JButton("Choose Hot PNG"), 
			choosePressed = new JButton("Choose Pressed PNG");
	Image nmlImg = null,
			hotImg = null,
			prsImg = null,
			AP_nmlImg = null,
			AP_prsImg = null;
			
	JLabel nmlLbl = new JLabel(),
			hotLbl = new JLabel(),
			prsLbl = new JLabel(),
			AP_nmlLbl = new JLabel(),
			AP_prsLbl = new JLabel(),
			AP_demo = new JLabel();
	JButton makeStart = new JButton("Create Start Button"),
			makeStartPNG = new JButton("Create Start Button");
	JFileChooser choose = new JFileChooser(),
				 nmlChooser = new JFileChooser(),
				 hotChooser = new JFileChooser(),
				 prsChooser = new JFileChooser(),
				 savChooser = new JFileChooser(),
				 AP_chooser = new JFileChooser();
	JSlider AP_fps = new JSlider(1, 30, 15);
	JSlider AP_time = new JSlider(1, 3000, 800);
	JSlider AP_target = new JSlider(0, 720, 360),
			gifLength = new JSlider(1, 3000, 800);
	JLabel AP_fps_lbl = new JLabel("FPS: " + AP_fps.getValue()),
			AP_time_lbl = new JLabel("Duration: " + AP_time.getValue() + "ms"),
			AP_target_lbl = new JLabel("Target in degrees: " + AP_target.getValue()),
			gifLengthLbl = new JLabel("Duration: " + gifLength.getValue() + "ms");
	File chosen = null,
		 nmlFile = null,
		 hotFile = null,
		 prsFile = null;
	BufferedImage[] AP_result = null;
	JScrollPane AP_result_gif = new JScrollPane();
	boolean needsToSave = false;
	public static void main(String args[]) {
		new Frame();
	}
	public BufferedImage[] spinImage(BufferedImage image, int fps, double time, int target) {
		double frames = (fps * (time/1000));
		BufferedImage[] spun = new BufferedImage[(int) (frames + 1)];
		for (int i = 0; i < spun.length; i++) spun[i] = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_ARGB);
		for (int i = 0; i <= frames; i++) {
			BufferedImage frame = spun[i];
			Graphics2D g2d = frame.createGraphics();
			// The required drawing location
			int drawLocationX = 0;
			int drawLocationY = 0;
			System.out.println(i + "   " + frames);
			// Rotation information
			double rotationRequired = Math.toRadians (((double)target / frames) * (double)i);
			double locationX = image.getWidth() / 2;
			double locationY = image.getHeight() / 2;
			AffineTransform tx = AffineTransform.getRotateInstance(rotationRequired, locationX, locationY);
			AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
			
			// Drawing the rotated image at the required drawing locations
			g2d.drawImage(op.filter(image, null), drawLocationX, drawLocationY, null);
		}
		System.out.println(Arrays.toString(spun));
		return spun;
	}
	GifImage gif = null;
	/**
	 * I hate every other developer in the world.
	 * @param out
	 * @return
	 */
	public BufferedImage getGif(BufferedImage... buf) {
		ByteArrayOutputStream str = new ByteArrayOutputStream();
		ImageOutputStream ios = null;
		try {
			ios = ImageIO.createImageOutputStream(str);
			Giffer.generateFromBI(buf, ios, this.AP_fps.getValue(), true);
			ios.flush();
		} catch (IIOException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		ByteArrayInputStream inp = new ByteArrayInputStream(str.toByteArray());
		try {
			return ImageIO.read(inp);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	
	
	/**
	 * Converts a given Image into a BufferedImage
	 *
	 * @param img The Image to be converted
	 * @return The converted BufferedImage
	 */
	public static BufferedImage toBufferedImage(Image img)
	{
	    if (img instanceof BufferedImage)
	    {
		   return (BufferedImage) img;
	    }

	    // Create a buffered image with transparency
	    BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

	    // Draw the image on to the buffered image
	    Graphics2D bGr = bimage.createGraphics();
	    bGr.drawImage(img, 0, 0, null);
	    bGr.dispose();

	    // Return the buffered image
	    return bimage;
	}
	public void example(final InputStream in) throws Exception {
	    final GifImage gif = GifDecoder .read(in);
	    gif.getWidth();
	    gif.getHeight();
	    gif.getBackgroundColor();
	    final int frameCount = gif.getFrameCount();
	    this.gif = gif;
	    for (int i = frameCount-1; i >= 0; i--) {
		   gif.getDelay(i);
		   System.out.println("Hello");
		   BufferedImage frame = this.gif.getFrame(i);
		   
		   frames.add(new JLabel(new ImageIcon(frame)));
		   frames.revalidate();
		   frames.repaint();
			scrollPane.repaint();
			pane.revalidate();
			pane.repaint();
	    }
	}
	public void setUpPNG() {
		p.gridx = 0;
		p.gridy = 0;
		p.weightx = 1;
		p.weighty = 1;
		p.anchor = GridBagConstraints.PAGE_START;
		p.fill = GridBagConstraints.HORIZONTAL;
		chooseNml.setPreferredSize(new Dimension(100, 50));
		chooseHot.setPreferredSize(new Dimension(100, 50));
		choosePressed.setPreferredSize(new Dimension(100, 50));
		makeStartPNG.setPreferredSize(new Dimension(100, 70));
		chooseNml.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				nmlChooser.setFileFilter(new FileFilter(){

					@Override
					public boolean accept(File pathname) {
						return pathname.getName().endsWith(".png") || pathname.isDirectory();
					}

					@Override
					public String getDescription() {
						return "Transparent Image Files (.png)";
					}
				
				});
				int choice = nmlChooser.showOpenDialog(frame);
				if (choice == JFileChooser.APPROVE_OPTION) {
					needsToSave = true;
					nmlFile = nmlChooser.getSelectedFile();
					try {
						nmlImg = ImageIO.read(nmlFile);
						nmlLbl.setIcon(new ImageIcon(nmlImg));
						if ( nmlImg.getHeight(null) > 300)
						nmlLbl.setIcon(new ImageIcon(nmlImg.getScaledInstance(
								(int) (nmlImg.getWidth(null) * (300.0 / nmlImg.getHeight(null))),
								300, Image.SCALE_SMOOTH)));
						if ( nmlImg.getWidth(null) > 300)
							nmlLbl.setIcon(new ImageIcon(nmlImg.getScaledInstance(
									300,
									(int) (nmlImg.getHeight(null) * (300.0 / nmlImg.getWidth(null))),
									 Image.SCALE_SMOOTH)));
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
			
		});
		chooseHot.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				hotChooser.setFileFilter(new FileFilter(){

					@Override
					public boolean accept(File pathname) {
						return pathname.getName().endsWith(".png") || pathname.isDirectory();
					}

					@Override
					public String getDescription() {
						return "Transparent Image Files (.png)";
					}
				
				});
				int choice = hotChooser.showOpenDialog(frame);
				if (choice == JFileChooser.APPROVE_OPTION) {
					needsToSave = true;
					hotFile = hotChooser.getSelectedFile();
					try {
						hotImg = ImageIO.read(hotFile);
						hotLbl.setIcon(new ImageIcon(hotImg));
						if ( hotImg.getHeight(null) > 300)
						hotLbl.setIcon(new ImageIcon(hotImg.getScaledInstance(
								(int) (hotImg.getWidth(null) * (300.0 / hotImg.getHeight(null))),
								300, Image.SCALE_SMOOTH)));
						if ( hotImg.getWidth(null) > 300)
							hotLbl.setIcon(new ImageIcon(hotImg.getScaledInstance(
									300,
									(int) (hotImg.getHeight(null) * (300.0 / hotImg.getWidth(null))),
									 Image.SCALE_SMOOTH)));
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
			
		});
		choosePressed.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				prsChooser.setFileFilter(new FileFilter(){

					@Override
					public boolean accept(File pathname) {
						return pathname.getName().endsWith(".png") || pathname.isDirectory();
					}

					@Override
					public String getDescription() {
						return "Transparent Image Files (.png)";
					}
				
				});
				int choice = prsChooser.showOpenDialog(frame);
				if (choice == JFileChooser.APPROVE_OPTION) {
					needsToSave = true;
					prsFile = hotChooser.getSelectedFile();
					try {
						prsLbl.setIcon(new ImageIcon(prsImg = ImageIO.read(prsFile)));
						if ( prsImg.getHeight(null) > 300)
							prsLbl.setIcon(new ImageIcon(prsImg.getScaledInstance(
									(int) (prsImg.getWidth(null) * (300.0 / prsImg.getHeight(null))),
									300, Image.SCALE_SMOOTH)));
							if ( prsImg.getWidth(null) > 300)
								prsLbl.setIcon(new ImageIcon(prsImg.getScaledInstance(
										300,
										(int) (prsImg.getHeight(null) * (300.0 / prsImg.getWidth(null))),
										 Image.SCALE_SMOOTH)));
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
			
		});
		makeStartPNG.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				if (nmlImg != null && hotImg != null && prsImg != null) {
					choose.addChoosableFileFilter(new FileFilter(){

						@Override
						public boolean accept(File f) {
							return f.getName().endsWith(".png") || f.isDirectory();
						}

						@Override
						public String getDescription() {
							return "Transparent image files (.png)";
						}
					});
					int option = savChooser.showSaveDialog(frame);
					if(option == JFileChooser.APPROVE_OPTION) {
						needsToSave = false;
						int width, height;
						
						if (nmlImg.getWidth(null) > hotImg.getWidth(null) && nmlImg.getWidth(null) > prsImg.getWidth(null))
							width = nmlImg.getWidth(null);
						else if (hotImg.getWidth(null) > nmlImg.getWidth(null) && hotImg.getWidth(null) > prsImg.getWidth(null))
							width = hotImg.getWidth(null);
						else if (prsImg.getWidth(null) > nmlImg.getWidth(null) && prsImg.getWidth(null) > hotImg.getWidth(null))
							width = prsImg.getWidth(null);
						else width = nmlImg.getWidth(null);
						
						if (nmlImg.getHeight(null) > hotImg.getHeight(null) && nmlImg.getHeight(null) > prsImg.getHeight(null))
							height = nmlImg.getHeight(null);
						else if (hotImg.getHeight(null) > nmlImg.getHeight(null) && hotImg.getHeight(null) > prsImg.getHeight(null))
							height = hotImg.getHeight(null);
						else if (prsImg.getHeight(null) > nmlImg.getHeight(null) && prsImg.getHeight(null) > hotImg.getHeight(null))
							height = prsImg.getHeight(null);
						else height = nmlImg.getHeight(null);
						
						
						BufferedImage buf  = new BufferedImage(width, height * 3, BufferedImage.TYPE_INT_ARGB);
						File file = savChooser.getSelectedFile();
						String filePath = file.getAbsolutePath();
						if(!filePath.toLowerCase().endsWith(".png")) {
						    file = new File(filePath + ".png");
						}
						Graphics2D gbuf = buf.createGraphics();
						gbuf.drawImage(nmlImg, 0, 0, null);
						gbuf.drawImage(hotImg, 0, height, null);
						gbuf.drawImage(prsImg, 0, height*2, null);
						try {
							ImageIO.write(buf, "png", file);
						} catch (IOException e4) {
							e4.printStackTrace();
						}
					}
				}
			}
			
		});
		PNGPane.add(chooseNml, p);
		p.gridx++;
		PNGPane.add(chooseHot, p);
		p.gridx++;
		PNGPane.add(choosePressed, p);
		p.gridx = 0;
		p.gridy++;
		PNGPane.add(nmlLbl, p);
		p.gridx++;
		PNGPane.add(hotLbl, p);
		p.gridx++;
		PNGPane.add(prsLbl, p);
		p.anchor = GridBagConstraints.PAGE_END;
		p.gridy++;
		p.gridx = 0;
		p.gridwidth = 3;
		PNGPane.add(makeStartPNG, p);
	}
	public void setUpGIF() {
		gifLength.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				gifLengthLbl.setText("Duration: " + gifLength.getValue() + "ms");
			}
			
		});
		choose.setAcceptAllFileFilterUsed(false);
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 1;
		c.weighty = 0;
		c.fill = GridBagConstraints.BOTH;
		
		chooseGif.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				
				choose.setFileFilter(new FileFilter(){

					@Override
					public boolean accept(File pathname) {
						return pathname.getName().endsWith(".gif") || pathname.isDirectory();
					}

					@Override
					public String getDescription() {
						return "Animated Image Files (.gif)";
					}
				
				});
				int choice = choose.showOpenDialog(frame);
				if (choice == JFileChooser.APPROVE_OPTION) {
					needsToSave = true;
					try {
						chosen = choose.getSelectedFile();
						InputStream in = new FileInputStream(chosen);
						try {
							example(in);
						} catch (Exception e1) {
							e1.printStackTrace();
						}
				//		gif = new BufferedImage[tempGif.getFrameCount()];
				//		for (int i = 0; i < tempGif.getFrameCount(); i++) {
				//			gif[i] = tempGif.getFrame(i);
				//		}
					} catch (FileNotFoundException e1) {
						e1.printStackTrace(); //Should never occur, but whatever.
					}
				}
			}
		});
		makeStart.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				if (gif != null) {
					for (FileFilter ff : choose.getChoosableFileFilters())
					choose.removeChoosableFileFilter(ff);
					choose.addChoosableFileFilter(new FileFilter(){

						@Override
						public boolean accept(File f) {
							return f.getName().endsWith(".png") || f.isDirectory();
						}

						@Override
						public String getDescription() {
							return "Transparent image files (.png)";
						}
						
					});
					System.out.println(chosen.toString());
					String change = chosen.getAbsolutePath();
					System.out.println(chosen.toString() + "   " + change);
					change = Regex.removeRegex(change, ".(\\.gif)", 1) + ".png";
					choose.setSelectedFile(new File(change));
					int option = choose.showSaveDialog(frame);
					if(option == JFileChooser.APPROVE_OPTION) {
						needsToSave = false;
						BufferedImage buf  = new BufferedImage(gif.getWidth(), gif.getHeight() * gif.getFrameCount() + 1, BufferedImage.TYPE_INT_ARGB);
						File file = choose.getSelectedFile();
						String filePath = file.getAbsolutePath();
						if(!filePath.toLowerCase().endsWith(".png")) {
						    file = new File(filePath + ".png");
						}
						Graphics2D gbuf = buf.createGraphics();
						drawPoint(gbuf, 0, 0, new Color(65, 78, 77));	//These four lines add the information pixels classic shell needs to know that this file is an animation.
																		//The Color of the first pixel says "ANM" in ascii, the second says "BTN".
						drawPoint(gbuf, 1, 0, new Color(66, 84, 78));	//Its stupid but necessary.
						drawPoint(gbuf, 2, 0, new Color(1, 0, gif.getFrameCount()));
						drawPoint(gbuf, 3, 0, new Color(0, 0, 0));
						drawPoint(gbuf, 4, 0, new Color(0, 0, gif.getFrameCount() - 2));
						drawPoint(gbuf, 5, 0, new Color(0, 0, gif.getFrameCount() - 1));
						if (demoPane.getSelectedComponent().getName().equals("ntho") || demoPane.getSelectedComponent().getName().equals("both"))
							drawPoint(gbuf, 5 + 1, 0, new Color(0, 1, (int)(((double)gifLength.getValue()) / (50.0/3.0))  ));
						else drawPoint(gbuf, 5 + 1, 0, new Color(0, 1, 0));
						drawPoint(gbuf, 5 + 2, 0, new Color(0, 0, gif.getFrameCount() - 2));
						if (demoPane.getSelectedComponent().getName().equals("htno") || demoPane.getSelectedComponent().getName().equals("both"))
							drawPoint(gbuf, 5 + 3, 0, new Color(0, 1, (int)(((double)gifLength.getValue()) / (50.0/3.0))  ));
						drawPoint(gbuf, 5 + 4, 0, new Color(gif.getFrameCount() - 2, 0, 0));
						drawPoint(gbuf, 5 + 5, 0, new Color(0, 0, 0));
						drawPoint(gbuf, 5 + 6, 0, new Color(0, 0, 0));
						drawPoint(gbuf, 5 + 7, 0, new Color(0, 0, 0));
						drawPoint(gbuf, 5 + 8, 0, new Color(0, 0, 0));
						for (int i = 0; i < gif.getFrameCount(); i++) {
							BufferedImage pic = gif.getFrame(i);
							gbuf.drawImage(pic, 0, gif.getHeight() * i + 1, null);
						}
						try {
							ImageIO.write(buf, "png", file);
						} catch (IOException e4) {
							e4.printStackTrace();
						}
					}
				}
			}
			
		});
		pane.add(demoPane, c);

		   c.gridx+= 3;
	   pane.add(chooseGif, c);
	   nthoPn.add(new JLabel("<html>Choose from the options above how you want the button to animate!</html>"));
	   nthoPn.add(ntho);
	   htnoPn.add(new JLabel("<html>Choose from the options above how you want the button to animate!</html>"));
	   htnoPn.add(htno);
	   bothPn.add(new JLabel("<html>Choose from the options above how you want the button to animate!</html>"));
	   bothPn.add(both);
	   nonePn.add(new JLabel("<html>Choose from the options<br>above how you want the button to animate!</html>"));
	   nonePn.add(none);
	   demoPane.addTab("Normal to Hot", nthoPn);
	   nthoPn.setName("ntho");
	   demoPane.addTab("Hot to Normal", htnoPn);
	   htnoPn.setName("htno");
	   demoPane.addTab("Both", bothPn);
	   bothPn.setName("both");
	   demoPane.addTab("No Animation", nonePn);
	   nonePn.setName("none");
	   c.gridy++;	
	   c.gridx = 0;
	   c.gridwidth = 1;
	   c.gridheight = 2;
	   c.weighty = 8;
	   c.weightx = 2;
	   scrollPane.getVerticalScrollBar().setUnitIncrement(16);
	   pane.add(scrollPane, c);
	   c.weighty = 1;
	   c.weightx = 1;
	   c.gridx++;
	   c.gridheight = 3;
	   c.gridy--;
	   c.fill = GridBagConstraints.BOTH;
	   gifLength.setOrientation(JSlider.VERTICAL);
	   pane.add(gifLength, c);
	   c.gridx++;
	   pane.add(gifLengthLbl, c);
	   c.gridheight = 1;
	   c.gridx++;
	   c.gridy++;
	   pane.add(makeStart, c);
	   demoPane.setSelectedComponent(bothPn);
    
	}
	public void setUpAnimatePNG() {
		AP_constructBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				if (AP_chooser.showSaveDialog(frame) == JFileChooser.APPROVE_OPTION) {			
					
					needsToSave = false;
					BufferedImage buf  = new BufferedImage(AP_result[0].getWidth(), AP_result[0].getHeight() * (AP_result.length + 1) + 1, BufferedImage.TYPE_INT_ARGB);
					File file = AP_chooser.getSelectedFile();
					String filePath = file.getAbsolutePath();
					if(!filePath.toLowerCase().endsWith(".png")) {
						file = new File(filePath + ".png");
					}
					Graphics2D gbuf = buf.createGraphics();
					drawPoint(gbuf, 0, 0, new Color(65, 78, 77));	//These four lines add the information pixels classic shell needs to know that this file is an animation.
																	//The Color of the first pixel says "ANM" in ascii, the second says "BTN".
					drawPoint(gbuf, 1, 0, new Color(66, 84, 78));	//Its stupid but necessary.
					drawPoint(gbuf, 2, 0, new Color(1, 0, AP_result.length + 1));
					drawPoint(gbuf, 3, 0, new Color(0, 0, 0));
					drawPoint(gbuf, 4, 0, new Color(0, 0, AP_result.length - 1));
					drawPoint(gbuf, 5, 0, new Color(0, 0, AP_result.length));
					drawPoint(gbuf, 5 + 1, 0, new Color(0, 1, (int)(((double)AP_time.getValue()) / (50.0/3.0))  ));
					drawPoint(gbuf, 5 + 2, 0, new Color(0, 0, AP_result.length - 1));
					drawPoint(gbuf, 5 + 3, 0, new Color(0, 1, (int)(((double)AP_time.getValue()) / (50.0/3.0))  ));
					drawPoint(gbuf, 5 + 4, 0, new Color(AP_result.length - 1, 0, 0));
					drawPoint(gbuf, 5 + 5, 0, new Color(0, 0, 0));
					drawPoint(gbuf, 5 + 6, 0, new Color(0, 0, 0));
					drawPoint(gbuf, 5 + 7, 0, new Color(0, 0, 0));
					drawPoint(gbuf, 5 + 8, 0, new Color(0, 0, 0));
					for (int i = 0; i < AP_result.length; i++) {
						BufferedImage pic = AP_result[i];
						gbuf.drawImage(pic, 0, AP_result[0].getHeight() * i + 1, null);
					}
					gbuf.drawImage(AP_prsImg, 0, AP_result[0].getHeight() * AP_result.length, null);
					try {
						ImageIO.write(buf, "png", file);
					} catch (IOException e4) {
						e4.printStackTrace();	 
					}
				}
			}
			
		});
		a.fill = GridBagConstraints.HORIZONTAL;
		a.weighty = 1;
		a.weightx = 1;
		a.gridx = 0;
		a.gridy = 0;
		a.anchor = GridBagConstraints.PAGE_START;
		a.gridwidth = 4;
		PNGSpin.add(new JLabel("<html> <h1 color = #FF0000> WARNING: Image MUST be either a circle or smaller than the total image! Otherwise, the animation WILL clip! </h1> </html>"), a);
		a.gridy++;
		a.gridwidth = 2;
		AP_chooser.addChoosableFileFilter(new FileFilter() {

			@Override
			public boolean accept(File f) {
				return f.isDirectory() || f.getName().endsWith(".png");
			}

			@Override
			public String getDescription() {
				return "Transparent Image Files (.png)";
			}
		
		});
		AP_chooser.setMultiSelectionEnabled(false);
		AP_chooser.setAcceptAllFileFilterUsed(false);
		AP_normalBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int result = AP_chooser.showOpenDialog(frame);
				if (result == JFileChooser.APPROVE_OPTION) {
					File chosen = AP_chooser.getSelectedFile();
					try {
						AP_nmlImg = ImageIO.read(chosen);
						if ( AP_nmlImg.getHeight(null) > 300)
						AP_nmlImg = AP_nmlImg.getScaledInstance(
								(int) (AP_nmlImg.getWidth(null) * (300.0 / AP_nmlImg.getHeight(null))),
								300, Image.SCALE_SMOOTH);
						if ( AP_nmlImg.getWidth(null) > 300)
							AP_nmlImg = AP_nmlImg.getScaledInstance(
									300,
									(int) (AP_nmlImg.getHeight(null) * (300.0 / AP_nmlImg.getWidth(null))),
									Image.SCALE_SMOOTH);
						AP_nmlLbl.setIcon(new ImageIcon(AP_nmlImg));
						AP_result = spinImage(toBufferedImage(AP_nmlImg), AP_fps.getValue(), AP_time.getValue(), AP_target.getValue());
						AP_result_gif.removeAll();
						
						for (BufferedImage frame : AP_result) {
							AP_result_gif.add(new JLabel(new ImageIcon(frame)));
							PNGSpin.revalidate();
							PNGSpin.repaint();
						}
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			} 
			
		});
		AP_pressedBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int result = AP_chooser.showOpenDialog(frame);
				if (result == JFileChooser.APPROVE_OPTION) {
					File chosen = AP_chooser.getSelectedFile();
					try {
						AP_prsImg = ImageIO.read(chosen);
						AP_prsLbl.setIcon(new ImageIcon(AP_prsImg));
						if ( AP_prsImg.getHeight(null) > 300)
						AP_prsLbl.setIcon(new ImageIcon(AP_prsImg.getScaledInstance(
								(int) (AP_prsImg.getWidth(null) * (300.0 / AP_prsImg.getHeight(null))),
								300, Image.SCALE_SMOOTH)));
						if ( AP_prsImg.getWidth(null) > 300)
							AP_prsLbl.setIcon(new ImageIcon(AP_prsImg.getScaledInstance(
									300,
									(int) (AP_prsImg.getHeight(null) * (300.0 / AP_prsImg.getWidth(null))),
									 Image.SCALE_SMOOTH)));
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
			
		});

		PNGSpin.add(AP_normalBtn, a);
		a.gridx += 2;
		PNGSpin.add(AP_pressedBtn, a);
		a.gridy++;
		a.gridx = 0;
		PNGSpin.add(AP_nmlLbl, a);
		a.gridx += 2;
		PNGSpin.add(AP_prsLbl, a);
		a.gridwidth = 1;
		a.weightx = 0.8;
		a.gridx = 0;
		a.gridy = 3;
		AP_target.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				AP_target_lbl.setText("Target in degrees: " + AP_target.getValue());
			}
			
		});
		AP_time.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				AP_time_lbl.setText("Duration: " + AP_time.getValue() + "ms");
			}
			
		});
		AP_fps.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				AP_fps_lbl.setText("FPS: " + AP_fps.getValue());
			}
			
		});
		PNGSpin.add(AP_time, a);
		a.gridx ++;
		a.weightx = 0.2;
		PNGSpin.add(AP_time_lbl, a);
		a.weightx = 0.8;
		a.gridx++;
		PNGSpin.add(AP_fps, a);
		a.weightx = 0.2;
		a.gridx++;
		PNGSpin.add(AP_fps_lbl, a);
		a.weightx = 0.8;
		a.gridx = 0;
		a.gridwidth = 3;
		a.gridy++;
		PNGSpin.add(AP_target, a);
		a.weightx = 0.2;
		a.gridx += 3;
		a.gridwidth = 1;
		PNGSpin.add(AP_target_lbl, a);
		a.weighty = 8;
		a.gridx = 0;
		a.gridwidth = 3;
		a.gridy++;
		a.fill = GridBagConstraints.BOTH;
		PNGSpin.add(AP_result_gif, a);
		a.gridwidth = 1;
		a.gridx += 3;
		PNGSpin.add(AP_constructBtn, a);
	}	
	public static void changeFont ( Component component, Font font )
	{
		component.setFont ( font );
		if ( component instanceof Container )
		{
			for ( Component child : ( ( Container ) component ).getComponents () )
			{
				changeFont ( child, font );
			}
		}	
	}
	public Frame() {
		setUpPNG();
		setUpGIF();
		setUpAnimatePNG();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 600);
		metaPane.setTabPlacement(JTabbedPane.BOTTOM);
		metaPane.add("GIF -> Animated Icon", pane);
		metaPane.add("PNG -> Normal Icon", PNGPane);
		metaPane.add("PNG -> Animated Icon", PNGSpin);
			frame.setContentPane(trueMetaPane);
			trueMetaPane.add(metaPane);
		  	
	   try {
		   updateLookAndFeel("noire");
		} catch (ClassNotFoundException e2) {
			e2.printStackTrace();
		} catch (InstantiationException e2) {
			e2.printStackTrace();
		} catch (IllegalAccessException e2) {
			e2.printStackTrace();
		} catch (UnsupportedLookAndFeelException e2) {
			e2.printStackTrace();
		}
		changeFont(frame, new Font("Arial", Font.PLAIN, 16));
	   frame.setVisible(true);
	}
	public static void drawPoint(Graphics2D g, int x, int y, Color clr) {
		Color temp = g.getColor();
		g.setColor(clr);
		g.drawLine(x, y, x, y);
		g.setColor(temp);
	}
	public void updateLookAndFeel(String jtattooLook) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		jtattooLook = jtattooLook.toLowerCase();
		String firstLetterCap = Character.toUpperCase(jtattooLook.charAt(0)) + jtattooLook.substring(1);
		UIManager.setLookAndFeel("com.jtattoo.plaf." + jtattooLook + "." + firstLetterCap + "LookAndFeel");
		SwingUtilities.updateComponentTreeUI(frame);
		choose.updateUI();
	}
}
