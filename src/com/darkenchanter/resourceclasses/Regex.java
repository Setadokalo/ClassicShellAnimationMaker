package com.darkenchanter.resourceclasses;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex {
	protected final static boolean DEBUG = false;
	/** Returns the string, minus the regex-matched portion.
     * 
     * @param url
     */
	public static String removeRegex(String input, String regex, int group) {
		Pattern p = Pattern.compile(regex);
        // create matcher for the pattern.
        Matcher m = p.matcher(input);

        // if a match was found in the input...
        if (m.find()) { 
        	String s = input.substring(0, input.lastIndexOf(m.group(group)));
            return  s; // return the string without that group.
        }
        else return null; // Otherwise, return nothing.
    }
	
}
