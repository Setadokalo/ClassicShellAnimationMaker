# Classic Shell Animation Maker
#### [Download Here!](/uploads/139515d5afb69f306f9828d304efbfda/classicshelliconmaker.jar)

The process of creating animations in Classic Shell always seemed very daunting; Having to add 'ANM' 'BTN' to the beginning, using color to denote the position of frames, row management; It seemed like the very opposite of fun. So, I decided to automate the process. Animation can now be made simply by providing a gif, choosing between the simple options, and pressing save. Or, for static images, you can make them spin to a certain rotation. This is all made easy, as you just press the Choose button, select your file, and save the result! 

##### Stuff used to make this:

 * [Dhyan's GifDecoder](https://github.com/DhyanB/Open-Imaging) for reading the frames of gifs.
